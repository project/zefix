<?php

namespace Drupal\zefix\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure zefix settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zefix_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['zefix.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['field_environment'] = [
      '#type' => 'select',
      '#required' => 'TRUE',
      '#title' => 'Umgebung',
      '#default_value' => $this->config('zefix.settings')->get('environment'),
      '#options' => [
        'test' => $this->t('Testumgebung'),
        'production' => $this->t('Produktivumgebung'),
      ],
    ];
    $form['field_user'] = [
      '#type' => 'textfield',
      '#required' => 'TRUE',
      '#title' => 'Benutzer',
      '#default_value' => $this->config('zefix.settings')->get('user'),
    ];
    $form['field_password'] = [
      '#type' => 'textfield',
      '#required' => 'TRUE',
      '#title' => 'Passwort',
      '#default_value' => $this->config('zefix.settings')->get('password'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('zefix.settings')
      ->set('environment', $form_state->getValue('field_environment'))
      ->set('user', $form_state->getValue('field_user'))
      ->set('password', $form_state->getValue('field_password'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
