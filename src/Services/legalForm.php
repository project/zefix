<?php

namespace Drupal\zefix\Services;

class legalForm {
  protected $config;
  private $TEST_BASE_URL = 'https://www.zefixintg.admin.ch/ZefixPublicREST/api/v1/';
  private $PROD_BASE_URL = 'https://www.zefix.admin.ch/ZefixPublicREST/api/v1/';

  public function __construct() {
    $this->config = \Drupal::configFactory()->get('zefix.settings');
  }

  /**
   * Get a list of all legal forms and their codes
   *
   * @link https://www.zefixintg.admin.ch/ZefixPublicREST/swagger-ui/index.html#/LegalForm/list_1
   */
  public function legalForm(): ?array {
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->config->get('environment') == 'test' ? $this->TEST_BASE_URL . 'legalForm' : $this->PROD_BASE_URL . 'legalForm',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_USERPWD =>  $this->config->get('user') . ':' . $this->config->get('password')
    ));

    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
    curl_close($curl);

    if($httpcode == 200){
      return json_decode($response, true);
    } else {
      \Drupal::logger('zefix')->error($httpcode . ' - ' . json_encode($response));
      return null;
    }
  }
}
